# Another Site Scraper?! 

Eh... sorta. This is more a boilerplate project that takes care of some tricky things before the actual scraping.   
Eg: 
- Loading/Parsing sitemaps into a flat array of urls is a big thing. And even bigger if there are linked sitemaps AND you need to be polite to the remote host. This handles that.  
The 'load sitemaps in series' bit is abstracted so it can be used later on to be polite to the remote. Promise.all() is great - but if that means hammering a prospective client's live site with thousands of web requests.... .... ....ehhhhh. 
    
