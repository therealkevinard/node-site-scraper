/**
 * Execute promises in serial, returning collected output
 * @param taskList
 *  Array of functions that return promises.
 * @param initial
 * @return {*}
 */
const promiseReducer = (taskList, initial) => {
    return taskList.reduce((curr, next) => {
        return curr.then(next)
    }, Promise.resolve(initial));
}
module.exports = promiseReducer;