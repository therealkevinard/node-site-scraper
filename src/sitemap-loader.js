/**
 * SitemapLoader Module
 */

const axios = require('axios');
const xml2js = require('xml2js');
const parser = new xml2js.Parser();
//---------
const promiseReducer = require('./promise-reducer');

//------------------------------------------- Internals
/**
 * Single-responsibility: provided xml string, parse into js object
 * @param xml
 * @return {Promise<*>}
 */
const parseRawXML = async (xml) => {
    return new Promise((resolve) => {
        parser.parseString(xml, (err, res) => {
            if (err) {
                throw new Error('xml parse error')
            }
            else {
                resolve(res)
            }
        })
    })
}

/**
 * Single-responsibility: load url and resolve response data if valid.
 * @param url
 * @return {Promise<*>}
 */
const loadXmlFile = async (url) => {
    let response = await axios.get(url);
    return new Promise((resolve, reject) => {
        if (response.status === 200 && response.data.length) {
            resolve(response.data)
        }
        else {
            throw new Error('failed to load sitemap')
        }
    })
}

/**
 * Purpose-built wrapper for promiseReducer to resolve a flat array.
 * @param requestChain
 * @return {Promise<*>}
 */
const loadSeries = async (requestChain) => {
    return new Promise(resolve => {
        promiseReducer(requestChain, [])
            .then((collected) => {
                resolve(collected);
            })
    })
}

//------------------------------------------- Workers

/**
 * Given a url to an xml file, loads and parses the response, returning natural object
 * @param url
 * @return {Promise<*>}
 */
const xmlFromUrl = async (url) => {
    let content = await loadXmlFile(url);
    let xml = await parseRawXML(content);
    return new Promise(resolve => {
        resolve(xml)
    })
}

/**
 * Given a sitemap index, collect the linked xml files into a tasklist for loadSeries
 * @param sitemapindex
 * @return {Promise<*>}
 */
const collectLinkedSitemaps = async (sitemapindex) => {
    let linkedFileRequests = sitemapindex.sitemap.map(node => {
        /*
        [For sequential requests] return reduce-friendly fn that takes previos collected responses and resolves with current appended.
         */
        return (collected) => {
            return new Promise(async resolve => {
                let singleXml = await xmlFromUrl(node.loc[0]);
                let singleUrls = await extractUrls(singleXml.urlset);
                resolve([
                    ...collected,
                    ...singleUrls
                ]);
            })
        }
    });
    return await loadSeries(linkedFileRequests);
}

/**
 * Single-responsibility: give a sitemap urlset, return only the browsable urls
 * @param xml
 *  urlset xmlnode: eg: contains url[n]
 * @return {Promise<void>}
 */
const extractUrls = async (xml) => {
    let urls = xml.url.map(url => {
        return url.loc[0]
    });
    return Promise.resolve(urls);
}

//------------------------------------------- Entry
/**
 * @param url
 *  remote path to sitemap. accepts either single or indexed xml.
 *      indexed xmls will be resolved in series, returning a flat array (similar to single xml)
 * @return {Promise<Array>}
 */
const main = async (url) => {
    let xml = await xmlFromUrl(url);
    let urls = [];
    if (xml.sitemapindex) {
        urls = await collectLinkedSitemaps(xml.sitemapindex)
    }
    else if (xml.urlset){
        urls = await extractUrls(xml.urlset);
    }
    return Promise.resolve(urls);
}

module.exports = main;